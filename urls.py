# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from aldryn_django.utils import i18n_patterns
import aldryn_addons.urls

from synoptic_cms_plugins import views

urlpatterns = [
    # add your own patterns here
    url(r'^en/catalogue/$', views.gallery_index,
        name="gallery_index"),
    url(r'^en/catalogue/(?P<slug>[-\w]+)/$', views.filter_gallery,
        name="subject"),
    url(r'^en/contact/$', views.contact_view,
        name="contact"),
    url(r'^en/contact/success/$', views.contact_success,
        name="contact-success")
] + aldryn_addons.urls.patterns() + i18n_patterns(
    # add your own i18n patterns here
    *aldryn_addons.urls.i18n_patterns()  # MUST be the last entry!
)
