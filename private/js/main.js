/* ========== Init ScrollMagic ========== */
var controller= new ScrollMagic.Controller();

var lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazy"
});

$(function(){

  /* SCROLL TO TOP BUTTON */
  $('#scrollTop').click(function(){
    gsap.to(window, .2, {scrollTo: {y:$('.navbar')}});
  });


  /* CARD FLIP */
  gsap.set($(".c-card__inner"), {transformStyle:"preserve-3d", rotationX:-180});
  gsap.set($(".c-card__back"), {rotationX: -180});
  gsap.set([$(".c-card__front"), $(".c-card__back")], {backfaceVisibility:"hidden"});

  gsap.to($(".c-card__inner"), {duration: 2, rotationX:0, ease:Back.easeOut});


  var cardTL = new TimelineMax()
    .to($(".c-card__inner"), 2 , {rotationX:0, ease:Back.easeOut})
    .fromTo($('.c-card__back'), 2, {borderRadius: '0 30px 0 30px'} , {borderRadius: '30px 0 30px 0'}, '-=.8');


  /* HOME ANIMATIONS */

  /*  Banner */
  if ($('.jumbotron').length > 0) {
    var jumbotronTL = new TimelineMax()
      .fromTo($('.jumbotron'), 1, {alpha: 0} , {alpha: 1})
      .fromTo($('.jumbotron .container'), 1, {alpha: 0, y: -500} , {alpha: 1, y:0}, '-=1')

    var jumbotronScene = new ScrollMagic.Scene({
      	 triggerElement: '.jumbotron',
      	 triggerHook: 1,
      	 reverse: false,
       })
      	 .setTween(jumbotronTL)
      	 .addTo(controller);
  }

  /* BENEFITS */
  if ($('.c-benefits').length > 0) {
    var benefitsTL = new TimelineMax()
      .from(".c-benefits__item", 2, {scale:0.5, opacity:0, delay:0.5, ease:Elastic.easeOut, force3D:true, stagger: 0.1}, 0.2);


    var benefitsScene = new ScrollMagic.Scene({
      	 triggerElement: '.c-benefits',
      	 triggerHook: 1,
      	 reverse: false,
       })
      	 .setTween(benefitsTL)
      	 .addTo(controller);
  }

  /* FEATURES */

  if ($('.c-feature__item').length > 0) {

    $('.c-feature__item').each(function(index, el){
      var featureImgLeft = $(this).children('.c-feature__img--left');
      var featureImgRight = $(this).children('.c-feature__img--right');
      var featureText = $(this).children('.c-feature__text');

      var featureTL = new TimelineMax()
      .fromTo(featureText, .5, {alpha: 0} , {alpha: 1})
      .fromTo(featureImgLeft, .5, {alpha: 0, x: -50} , {alpha: 1, x: 0}, '-=.3')
      .fromTo(featureImgRight, .5, {alpha: 0, x: 50} , {alpha: 1, x: 0}, '-=.3')

      var featureScene = new ScrollMagic.Scene({
    	 triggerElement: el,
    	 triggerHook: .7,
    	 reverse: false,
     })
    	 .setTween(featureTL)
    	 .addTo(controller);
    });
  }

  /* BLOG LIST */
  if ($('.c-blog__list').length > 0) {
    var blogTL = new TimelineMax()
      .from(".c-blog__list .c-blog__item", 2, {scale:0.5, opacity:0, delay:0.5, ease:Elastic.easeOut, force3D:true, stagger: 0.1}, 0.2);


    var blogScene = new ScrollMagic.Scene({
      	 triggerElement: '.c-benefits',
      	 triggerHook: 1,
      	 reverse: false,
       })
      	 .setTween(blogTL)
      	 .addTo(controller);
  }



});
