# -*- coding: utf-8 -*-
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from synoptic_cms_plugins.models import (
    CardModel,
    BannerModel,
    BenefitsModel,
    FeatureModel,
)


@plugin_pool.register_plugin
class CardModelPlugin(CMSPluginBase):
    model = CardModel
    name = 'Card'
    render_template = 'synoptic_cms_plugins/card.html'
    allow_children = False


@plugin_pool.register_plugin
class BannerModelPlugin(CMSPluginBase):
    model = BannerModel
    name = 'Banner'
    render_template = 'synoptic_cms_plugins/banner.html'
    allow_children = False


@plugin_pool.register_plugin
class BenefitsModelPlugin(CMSPluginBase):
    model = BenefitsModel
    name = 'Benefits'
    render_template = 'synoptic_cms_plugins/benefits.html'
    allow_children = False


@plugin_pool.register_plugin
class FeatureModelPlugin(CMSPluginBase):
    model = FeatureModel
    name = 'Feature'
    render_template = 'synoptic_cms_plugins/feature.html'
    allow_children = False
