from django.shortcuts import render
from django.shortcuts import render, redirect, get_object_or_404

from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect

from synoptic_cms_plugins.models import (
    GalleryImageModel,
    ImageSubjectModel
)
from synoptic_cms_plugins.forms import ContactForm

# Create your views here.


def gallery_index(request):
    subjects = ImageSubjectModel.objects.all().order_by('id')
    images = GalleryImageModel.objects.all().order_by('id')

    context = {'images': images, 'subjects': subjects}
    return render(request, "gallery.html", context)


def filter_gallery(request, slug):
    subjects = ImageSubjectModel.objects.all().order_by('id')
    images = GalleryImageModel.objects \
        .filter(image_subjects__slug=slug) \
        .order_by('-id')

    context = {'images': images, 'subjects': subjects}
    return render(request, "gallery.html", context)


def contact_view(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['admin@example.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect("/contact/success")
    else:
        form = ContactForm()
    return render(request, "contact.html", {'form': form})


def contact_success(request):
    return render(request, "contactSuccess.html")
