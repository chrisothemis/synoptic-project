from django.apps import AppConfig


class SynopticCmsPluginsConfig(AppConfig):
    name = 'synoptic_cms_plugins'
