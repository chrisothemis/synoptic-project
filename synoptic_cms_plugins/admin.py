from django.contrib import admin

from synoptic_cms_plugins.models import (
    GalleryImageModel,
    ImageSubjectModel
)

# Register your models here.


class GalleryImageAdmin(admin.ModelAdmin):
    list_display = ['thumbnail']


class ImageSubjectAdmin(admin.ModelAdmin):
    subject_name = ['icon_choice']
    exclude = ['slug']


admin.site.register(GalleryImageModel, GalleryImageAdmin)
admin.site.register(ImageSubjectModel, ImageSubjectAdmin)
