from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from cms.models import CMSPlugin, Page
from filer.fields.image import FilerImageField
from djangocms_text_ckeditor.fields import HTMLField
from django.utils.safestring import mark_safe
from django.utils.six import text_type
from django.template.defaultfilters import slugify

# Create your models here.


@python_2_unicode_compatible
class CardModel(CMSPlugin):
    text = HTMLField()
    image = FilerImageField(
        null=True, blank=False,
        on_delete=models.SET_NULL)

    def __str__(self):
        return text_type("{}").format(self.image)


@python_2_unicode_compatible
class GalleryImageModel(models.Model):
    image = models.ImageField(upload_to="Gallery")
    image_subjects = models.ManyToManyField(
        'ImageSubjectModel',
        related_name="subjects",
        related_query_name="subject"
    )
    image_description = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Gallery Image'
        verbose_name_plural = 'Gallery'

    def __str__(self):
        return text_type("{}").format(self.image)

    # admin thumbnail
    def thumbnail(self):
        return mark_safe(
            '<img src="%s" style = "height: 50px;" />' % (self.image.url))


@python_2_unicode_compatible
class ImageSubjectModel(models.Model):
    subject_name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(unique=True)

    class Meta:
        verbose_name = 'Image Subject'
        verbose_name_plural = 'Image Subjects'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.subject_name)
        super(ImageSubjectModel, self).save(*args, **kwargs)

    def __str__(self):
        return text_type("{}").format(self.subject_name)

# PLACEHOLDER CONTENT
@python_2_unicode_compatible
class BannerModel(CMSPlugin):
    def __str__(self):
        return "Banner"


@python_2_unicode_compatible
class BenefitsModel(CMSPlugin):
    def __str__(self):
        return "Benefits"


@python_2_unicode_compatible
class FeatureModel(CMSPlugin):
    def __str__(self):
        return "Feature"
