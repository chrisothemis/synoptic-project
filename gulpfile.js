'use strict';

var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var maps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var watch = require('gulp-watch');

var paths = {
  styles: {
    src: ['private/sass/**/*.scss', 'private/sass/input.scss'],
    dest: 'static/css/'
  },
  scripts: {
    src: ['private/js/*.js'],
    dest: 'static/js/'
  }
};

// autoprefixer gets it's config from .browserlistrc
function styles() {
  return gulp.src(paths.styles.src)
    .pipe(maps.init())
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    // pass in options to the stream
    .pipe(rename({
      basename: 'output',
      suffix: '.min'
    }))
    .pipe(maps.write('../maps'))
    .pipe(gulp.dest(paths.styles.dest));
}

// don't uglify vendors, they tend to bug out
function vendorScripts() {
  return gulp.src([
              'private/js/vendor/modernizr.min.js',
              'private/js/vendor/jquery.min.js',
              'private/js/vendor/lazyload.min.js',
              'private/js/vendor/plugins.js',
              'private/js/vendor/popper.min.js',
              'private/js/vendor/bootstrap.min.js',
              'private/js/vendor/fancybox.min.js',
              'private/js/vendor/Greensock/gsap.min.js',
              'private/js/vendor/Greensock/ScrollToPlugin.min.js',
              'private/js/vendor/ScrollMagic/ScrollMagic.min.js',
              'private/js/vendor/ScrollMagic/animation.gsap.min.js',
            ])
    .pipe(concat('1-bundle.js'))
    .pipe(gulp.dest('private/js'));
}


// use cdns for now, so we concat only our files
function scripts() {
  return gulp.src(paths.scripts.src[0])
    .pipe(maps.init())
    .pipe(uglify())
    .pipe(concat('output.min.js'))
    .pipe(maps.write('../maps'))
    .pipe(gulp.dest(paths.scripts.dest));
}


function watchFiles() {
  gulp.watch(paths.styles.src, styles);
  gulp.watch(['private/js/vendor/**/*.js', 'private/js/vendor/*.js'], vendorScripts);
  gulp.watch(paths.scripts.src[0], scripts);
}



// define complex tasks
var watch = gulp.series(watchFiles);
var build = gulp.series(styles, vendorScripts, scripts);

// export tasks
exports.watch = watch;
exports.build = build;
/*
 * Define default task that can be called by just running `gulp` from cli
 */
exports.default = build;
